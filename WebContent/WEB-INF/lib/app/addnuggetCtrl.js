var addnuggetCtrl = function($scope, $uibModalInstance, $http, $location, $window, $cookies) {

	$scope.error = false;
		$scope.addnugget = function(){
			
		
			if ($scope.ntitle == null ||  $scope.ntitle == undefined || $scope.ntitle.replace(/\s/g, '').length === 0) 
			{ 
				$scope.error = true;
			} else 
				{
				$scope.error = false;
				 $uibModalInstance.close($scope.ntitle);
				}
		};
		
		$scope.close = function(){
		
			$uibModalInstance.dismiss('cancel');
	
		};

};

app.controller('addnuggetCtrl', addnuggetCtrl);
addnuggetCtrl.$inject = ['$scope', '$uibModalInstance', '$http', '$location', '$window','$cookies'];
