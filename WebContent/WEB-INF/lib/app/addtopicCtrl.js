 "use strict";
var addtopicCtrl = function ($scope, $http, $location, $window, $cookies, $uibModal,topicjson, Upload, $sce,$crypto,config) {
	
	$scope.stag = [];
	$scope.category = {};
	
	$scope.subcats=[];
	 
	$scope.home = function(){
	    localStorage.getItem("786a2y1e") !=null ?   $scope.decry = JSON.parse($crypto.decrypt(localStorage.getItem("786a2y1e"), config.key)) : $window.location.href = '#login'; 
	    if($window.innerWidth > 1024){
            $scope.secondwidth = +$window.innerWidth - +239;
          }else{
              $scope.secondwidth = +$window.innerWidth - +65;
          }
	   
        $scope.second = {'width':$scope.secondwidth};
        
      
        $(window).resize(function() {
     
        $scope.$apply(function() {
          $scope.windowWidth = $(window).width();
       
          if($scope.windowWidth < 1023){
              $scope.secondwidth = +$window.innerWidth - +65;
              $scope.second = {'width':$scope.secondwidth};
              $scope.sscreen = true;
              $scope.lscreen = false;
          }       
         
          if($scope.windowWidth > 1024 ){
             
              $scope.secondwidth = +$window.innerWidth - +224;
              $scope.second = {'width':$scope.secondwidth};
              $scope.sscreen = false;
              $scope.lscreen = true;
              console.log($scope.secondwidth);
          }
        });
      });
		$scope.nuggets = [];
		$scope.quizcount = '0';
		$scope.showcertdes = false;
		$scope.nuggetslen = '0';
		$scope.tduration = 0;
		$scope.tvid=0;
		$scope.tdoc=0
		$scope.tfq=0;
		$scope.tsq=0;
		$scope.tobj=0;
		$scope.folder = Math.round((new Date()).getTime() / 1000);
		$scope.orgid = $scope.decry.oid;
		$scope.name = $scope.decry.username;

		$scope.topics = JSON.parse(topicjson);
		$scope.ctype = $scope.decry.ctype;
		
		$scope.decry["folder"] =  $scope.folder;
		$scope.decry["quizcount"] =  '0';
		localStorage.setItem("786a2y1e",$crypto.encrypt(JSON.stringify($scope.decry), config.key));
		/* localStorage.setItem("folder", $scope.folder);
		 localStorage.setItem("quizcount", '0');*/
		
		 $scope.noimage = false;
		 $scope.topicdata = {"tdescription":"","freenav":"","selection":"","hascert":false,"adprods":"","mrtools":"","prerequisites":"","prerequisitesCourses":""};
			$scope.content = false ;
			$scope.overview = true;
			$scope.settings = false;
		  $scope.bimage = '';
		  $scope.myObj = {'background-color':'grey','overflow':'hidden','background-image':$scope.bimage,'background-size': 'cover','background-repeat': 'no-repeat'};		
	};
	
	$scope.home();
	
	$scope.showcertdesc = function(value){
		if(value == 1){
			$scope.showcertdes = true;
		}else{
			$scope.showcertdes = false;
		}
		
	}
	$scope.optedpayment = function(value){
		if(value == 1){
			$scope.showPayment = true;
		}else{
			$scope.showPayment = false;
		}
		
	}
	$scope.onlyNumbers = function(event){   
	    var keys={
	        'up': 38,'right':39,'down':40,'left':37,
	        'escape':27,'backspace':8,'tab':9,'enter':13,'del':46,
	        '0':48,'1':49,'2':50,'3':51,'4':52,'5':53,'6':54,'7':55,'8':56,'9':57
	    };
	    for(var index in keys) {
	        if (!keys.hasOwnProperty(index)) continue;
	        if (event.charCode==keys[index]||event.keyCode==keys[index]) {
	            return; //default event
	        }
	    }   
	    event.preventDefault();
	};
	
	$scope.sectionchange = function(){
		if($scope.sectionselection != null &&  $scope.sectionselection == "Practice"){
			 $scope.noimage = true;
			 $scope.nomodule = true; 
		 }else if($scope.sectionselection != null &&  $scope.sectionselection == "Learn")
		 {
			 $scope.noimage = false;
			 $scope.nomodule = false; 
		 }else{
			 $scope.noimage = false; 
			 $scope.nomodule = true; 
		 }
	}
	
	$scope.discard = function(){
		swal({
			  title: 'Are you sure?',
			  text: "You won't be able to revert this!",
			  type: 'warning',
			  width: '400px',
			  customClass: 'sweetalert-lgs',
              buttonsStyling:false,allowOutsideClick: false,
              allowEscapeKey:false,
              showCancelButton: true, cancelButtonText: 'No', cancelButtonClass:"button2",
    		  showConfirmButton: true, confirmButtonText: 'Yes',  confirmButtonClass: 'button1'
			}).then((result) => {
			  if (result.value) {
				  $scope.loading = true;
				  $window.location.href = '#listtopics';
			  }
			})
	}
	
	$scope.savetopic = function() {
		swal({
			  title: 'Are you sure?',
			  text: "Save Story?",
			  type: 'warning',
			  width: '400px',
			  customClass: 'sweetalert-lgs',
              buttonsStyling:false,allowOutsideClick: false,
              allowEscapeKey:false,
              showCancelButton: true, cancelButtonText: 'No', cancelButtonClass:"button2",
    		  showConfirmButton: true, confirmButtonText: 'Yes',  confirmButtonClass: 'button1'
			}).then((result) => {
			  if (result.value) {
				  	$scope.loading = true;
					$scope.$apply();
					$scope.saveon();
			  } else if (
			    result.dismiss === swal.DismissReason.cancel
			  ) {
			  }
			})
	};
	
	$scope.optedfreenav = function(value){
		if(value == 0){
			
			$scope.showcert = true;	
		}else{
			$scope.showcert = false;
			$scope.topicdata.hascert='';
			$scope.topicdata.hascert=false;
			$scope.topicdata.ltype='false';
			$scope.ltype = 'regular';
			//$scope.topicdata.hascert = false;
		}
	}
	$scope.learntype = function(value){
		if(value == 0){
			
			$scope.ltype = 'regular';
			
		}else{
			$scope.ltype = 'enforce';
			//$scope.topicdata.hascert = false;
		}
	}
	
	
	$scope.topicimageupload = function(topicimg) {
		if(topicimg == null){
			return;
		}
		if(topicimg.size > 5242880){
			 swal({title: "Oops!", text: "Image size is too large. File size cannot be greater than 5mb.",type: "error",
				 buttonsStyling:false,allowOutsideClick: false,
	              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
			 return;
		}
		$scope.topicimage = topicimg;
		var topicimageid = { "image" : $scope.topicimage ,"imageid":"topic","typ":"uploadfile"};		
		$scope.commoncognito(topicimageid);
	};
	$scope.certimageupload = function(certimg) {
		if(certimg == null){
			return;
		}
		//5242880 1048576
		if(certimg.size > 5242880){
			 swal({title: "Oops!", text: "Image size is too large. File size cannot be greater than 5mb.",type: "error",
				 buttonsStyling:false,allowOutsideClick: false,
	              allowEscapeKey:false,width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
			 return;
		}
		
		var certimageid = { "image" : certimg ,"imageid":"cert","typ":"uploadfile"}; 
		$scope.commoncognito(certimageid);
		//$scope.uploadfile(certimg, certimageid);
	};
	$scope.saveon = function() {
		
		$scope.loading = true;
		
			if($scope.topicdata.ttitle == "" || $scope.topicdata.ttitle == undefined || $scope.topicdata.ttitle.replace(/\s/g, '').length === 0 ||
					$scope.topicdata.tdescription == "" || $scope.topicdata.tdescription == undefined || $scope.topicdata.tdescription.replace(/\s/g, '').length === 0 ||
					 $scope.topicdata.freenav == "" || $scope.topicdata.freenav == undefined ||  $scope.topicdata.topicimage == undefined || $scope.nuggets.length == 0
					 || $scope.topicdata.ltype == "" || $scope.topicdata.ltype == undefined
					 || $scope.topicdata.adprods === "" || $scope.topicdata.adprods === undefined || $scope.topicdata.mrtools == "" || $scope.topicdata.mrtools == undefined
					 || $scope.topicdata.prerequisites == "" || $scope.topicdata.prerequisites == undefined ){
				
				if($scope.topicdata.ttitle == "" || $scope.topicdata.ttitle == undefined || $scope.topicdata.ttitle.replace(/\s/g, '').length === 0){
					$scope.loading = false;
					swal({title: "Oops!", text: "Story title cannot be empty", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
				if($scope.topicdata.tdescription == "" || $scope.topicdata.tdescription == undefined || $scope.topicdata.tdescription.replace(/\s/g, '').length === 0){
					$scope.loading = false;
					swal({title: "Oops!", text: 'Story description cannot be empty in Overview section', type: "error",
						buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false,width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
				/*if($scope.topicdata.selection == "" || $scope.topicdata.selection == undefined){
					$scope.loading = false;
					swal({title: "Oops!", text: "Select categories to which the Topic belongs in Preferences section", type: "error", width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', confirmButtonColor: "#fb6142"});
					return;
				}*/
			/*	if($scope.topicdata.fees == "" || $scope.topicdata.fees == undefined){
					
					$scope.loading = false;
					swal({title: "Oops!", text: "Payment required should be either Yes or No", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', confirmButtonColor: "#fb6142",customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				
			}*/
				/*if($scope.topicdata.fees == true || $scope.topicdata.fees == 'true' && $scope.topicdata.price == "" || $scope.topicdata.price == undefined){
					$scope.loading = false;
					swal({title: "Oops!", text: "When payment required has been selected as Yes, price cannot be empty", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', confirmButtonColor: "#fb6142",customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}*/
				if($scope.topicdata.freenav == "" || $scope.topicdata.freenav == undefined){
					$scope.loading = false;
					swal({title: "Oops!", text: "Please select Navigation type in Preferences section", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
					
				}
				if($scope.topicdata.ltype == "" || $scope.topicdata.ltype == undefined){
					$scope.loading = false;
					swal({title: "Oops!", text: "Learning type should be selected as either Enforced or Regular.", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
				if($scope.topicdata.adprods == undefined || $scope.topicdata.adprods == ''){
					$scope.loading = false;
					swal({title: "Oops!", text: "Select Ad Products", type: "error",buttonsStyling:false,allowOutsideClick: false,
						  allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
				if($scope.topicdata.mrtools == undefined || $scope.topicdata.mrtools == ''){
					$scope.loading = false;
					swal({title: "Oops!", text: "Select Marketron Tools", type: "error",buttonsStyling:false,allowOutsideClick: false,
						  allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
				if($scope.topicdata.prerequisites == undefined || $scope.topicdata.prerequisites == ''){
					$scope.loading = false;
					swal({title: "Oops!", text: "Prerequisites should be selected as either Yes or No.", type: "error",buttonsStyling:false,allowOutsideClick: false,
						  allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
				
				if(($scope.topicdata.prerequisites == true || $scope.topicdata.prerequisites == 'true') && ($scope.topicdata.prerequisitesStorys === undefined || $scope.topicdata.prerequisitesCourses === '')){
					$scope.loading = false;
						swal({title: "Oops!", text: "You need to select Prerequisite Stories", type: "error",buttonsStyling:false,allowOutsideClick: false,
							  allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
						return;
					
				}
			if($scope.topicdata.freenav == false || $scope.topicdata.freenav == "false"){
				
				if($scope.topicdata.hascert == "" || $scope.topicdata.hascert == undefined){
					$scope.loading = false;
					swal({title: "Oops!", text: "Certification should be selected as either Yes or No.", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
			}
			
				if($scope.topicdata.topicimage == undefined){
					$scope.loading = false;
					swal({title: "Oops!", text: "Please select a Story image", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
				
				if($scope.nuggets.length == 0){
					$scope.loading = false;
					swal({title: "Oops!", text: "You need to add atleast 1 Module", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'Ok', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
			} else if($scope.topicdata.hascert == true || $scope.topicdata.hascert == 'true'){
				
				if($scope.ctype == 0 || $scope.ctype == '0' || $scope.ctype == undefined ){
					
				if($scope.topicdata.tauthority == "" || $scope.topicdata.tauthority == undefined || $scope.topicdata.tauthority.replace(/\s/g, '').length === 0){
				$scope.loading = false;
				swal({title: "Oops!", text: "Certification Authority cannot be empty", type: "error",buttonsStyling:false,allowOutsideClick: false,
		              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
				return;
				}
				if($scope.topicdata.tcredits == "" || $scope.topicdata.tcredits == undefined){
					$scope.loading = false;
					swal({title: "Oops!", text: "Credits cannot be empty", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
				if($scope.topicdata.tcredits != undefined && $scope.topicdata.tcredits <=0){
					$scope.loading = false;
					swal({title: "Oops!", text: "No. of Credits should be greater than Zero", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
				if($scope.topicdata.tempimage == undefined){
					$scope.loading = false;
					swal({title: "Oops!", text: "Please select a template for Certificate", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
				}else{
					//$scope.topicdata.tcredits = 0;
					//$scope.topicdata.tauthority = ' ';
					$scope.adid = ' ';
				}
				
				
				if($scope.nuggets.length > 0)
				{
					for (var i = 0; i < $scope.nuggets.length; i++) {
						 
						 if ($scope.nuggets[i].objects.length == '0') {
							 $scope.loading = false;
								swal({title: "Oops!", text: "You need to add atleast 1 Content in each Module", type: "error",buttonsStyling:false,allowOutsideClick: false,
						              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
								return;
						 }
						
					 }
					if($scope.topicdata.hascert == false || $scope.topicdata.hascert == 'false'){
						$scope.adid = ' ';
					}
					var typjson = {"typ" : 'submit'}
                    $scope.commoncognito(typjson)
				}else{
					$scope.loading = false;
					swal({title: "Oops!", text: "You need to add atleast 1 Module", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
					return;
				}
			}else {
				if($scope.nuggets.length > 0)
					{
						for (var i = 0; i < $scope.nuggets.length; i++) {
							 
							 if ($scope.nuggets[i].objects.length == '0') {
								 $scope.loading = false;
									swal({title: "Oops!", text: "You need to add atleast 1 Content in each Module", type: "error",buttonsStyling:false,allowOutsideClick: false,
							              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
									return;
							 }
						 }
						if($scope.topicdata.hascert == false || $scope.topicdata.hascert == 'false'){
							$scope.topicdata.tcredits = 0;
							$scope.topicdata.tauthority = ' ';
							$scope.adid = ' ';
						}
						var typjson = {"typ" : 'submit'}
						$scope.commoncognito(typjson)
					}else{
						$scope.loading = false;
						swal({title: "Oops!", text: "You need to add atleast 1 Module", type: "error",buttonsStyling:false,allowOutsideClick: false,
				              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
						return;
					}
			
			}
		
		 
	};
	$scope.commoncognito = function(typejson)
	{
	    AWSCognito.config.region =  config.reg;
        AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId: $scope.decry.iid
        });
      
        var poolData = { UserPoolId : $scope.decry.uid,
                ClientId : $scope.decry.cid
            };
        
        var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
        
        var cognitoUser = userPool.getCurrentUser();
        
        if (cognitoUser != null && $scope.decry.oid != null) {
            
            $scope.getsession(cognitoUser,typejson);
        }else {
            localStorage.clear();
            $window.location.href = '#login';
        }
	}
	$scope.getsession = function(cognitoUser,typejson){
        
        return new Promise((resolve, reject) => {
               cognitoUser.getSession((err, session) =>{
                  if (err) {
                      swal({title: "Oops!", text: "Session has timed out, Please login again.", type: "error",buttonsStyling:false,allowOutsideClick: false,
			              allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
                      localStorage.clear();
                      $window.location.href = '#login';
                  }else{
                      
                      var token = session.idToken.jwtToken;
                      if(typejson.typ == 'submit')
                      {
                          $scope.submit(token); 
                      }else if(typejson.typ == 'uploadfile')
                      {
                          $scope.uploadfile(typejson.image,typejson.imageid,token); 
                      }
                     
                 
              
                  }
              });
          })
  }
  $scope.loadContentPage = function(){
	if($scope.online == true){
		$scope.loading = true;
		$window.location.href = '#managecontent';
	}
	
};
$scope.loadCoursePage = function(){
	if($scope.online == true){
		$scope.loading = true;
		$window.location.href = '#listtopics';
	}
	
};
	$scope.submit = function(token) {
		
		var apigClient = apigClientFactory.newClient({ invokeUrl: $scope.decry.api,});
				var params = {};
				
				var body2 = {
						oid: $scope.orgid,
						NAME: $scope.name,
						TOPICNAME: $scope.topicdata.ttitle,
						eid:$scope.decry.email
					
						 };
				
				if($scope.topicdata.freenav == false){
					$scope.hascert = "false";
				}else{
					$scope.hascert = $scope.topicdata.hascert;
				}
			
				 
				var additionalParams = { 
				        headers: {Authorization : token
                }
				};
				$scope.taglists=[];
				for (var i = 0; i < $scope.stag.length; i++) {
					$scope.taglists.push($scope.stag[i].text);
				}
				$scope.category.id =$scope.decry.tid;
				$scope.category.name =$scope.decry.tname;
				$scope.category.ttags=$scope.taglists;
				body2.category = $scope.category;
				
				var topicidcall =	apigClient.getTopicIDPost(params, body2, additionalParams)
				.then(function(result){
				    	
				   var topicid = JSON.stringify(result.data);
				   var abc = topicid.slice(1, -1);
				   topicid = abc;
				   $scope.adid = ' ';
				   
				   var body3 = {
						   oid: $scope.orgid,
						   folder: $scope.folder,
						   tid: topicid,
						   ttitle: $scope.topicdata.ttitle,
						   tdescription: $scope.topicdata.tdescription,
						   freenavigation: $scope.topicdata.freenav,
						   certification: $scope.hascert,
						   tcertauth: $scope.topicdata.tauthority,
						   tnoofcredits: $scope.topicdata.tcredits,
						   tduration: $scope.tduration,
						   adid: $scope.adid,
						   cby: $scope.name,
						   noofnuggets: $scope.nuggets.length,
						   nuggets: $scope.nuggets,
						   tobj: $scope.tobj,
						   ctype: $scope.ltype,
						   tvid: $scope.tvid,
						   tdoc: $scope.tdoc,
						   tfq: $scope.tfq,
						   tsq: $scope.tsq,
						   tasmt: "0",
						   adprods: $scope.topicdata.adprods,
						   mrtools:$scope.topicdata.mrtools,
						   catlist:$scope.category,
						   prerequisites:$scope.topicdata.prerequisites,
						   prerequisitescourses:$scope.topicdata.prerequisitesCourses
					};
					
				  var createtopic =	apigClient.addTopicPost(params, body3, additionalParams)
					.then(function(result){ 
						
						$window.location.href = '#listtopics';
						
					}).catch( function(result){
				    	
				    	var json = JSON.stringify(result);
				    	var json1 = json.toString();
				    	alert('ERROR'+json1);
				    	
				    	
				    });
				 
				    }).catch( function(result){
				    	
				    	var json = JSON.stringify(result);
				    	var json1 = json.toString();
				    	alert('ERROR'+json1);
				    	
				    	
				    });
		};
	
	
	
	$scope.status = {
			    isFirstOpen: false,
			    isFirstDisabled: false,
			    isLasttOpen: false,
			  };
	 
	$scope.addnugget = function() {
		  $scope.Instance = $uibModal.open({
				templateUrl: 'addnugget.html',
				controller: 'addnuggetCtrl',
				backdrop: 'static',
		        keyboard: false,
		        windowClass: 'addnuggetmodal',
		        scope: $scope,
		        resolve: {
		            items: function () {
		            	
		              return $scope.object;
		            }
		          }
				});
		 $scope.Instance.result.then(function (ntitle) {
			
			if (ntitle != null && ntitle != undefined){
								 
				  $scope.objects = [];
				  $scope.nuggetslen++;
				  $scope.showaccordion = true;
				  $scope.item = {
								nid: $scope.nuggetslen,  
						        ntitle: ntitle,
						        nduration: 0,
						        objects:   $scope.objects
						    }
				  $scope.nuggets.push($scope.item);
					// $scope.$apply();			
									
				}
 
		    }, function () {
		     // alert($scope.object);
		    });
		 
		};
		$scope.addobject = function(nid) {
		
		 $scope.Instance = $uibModal.open({
				templateUrl: 'addobject.html',
				controller: 'addobjectCtrl',
				backdrop: 'static',
		        keyboard: false,
		        windowClass: 'addobjectmodal',
		        scope: $scope,
		        resolve: {
		            items: function () {
		            	
		              return $scope.object;
		            }
		          }
				});
		 $scope.Instance.result.then(function (selectedItem) {
			 var id = +nid - 1;
			 var json1 = JSON.stringify(selectedItem);
			 
			  $scope.object1 = JSON.parse(json1);
			  $scope.nuggets[id].objects.push($scope.object1);
			  
			  
			  if($scope.object1.otype=='audio'|| $scope.object1.otype=='video' || $scope.object1.otype=='youtube'|| $scope.object1.otype=='vimeo')
				  {
				  $scope.tvid=$scope.tvid+1;
				  }
			  if($scope.object1.otype=='html'|| $scope.object1.otype=='pdf' || $scope.object1.otype=='doc')
			  {
			  
			  $scope.tdoc=$scope.tdoc+1;
			  }
			  if($scope.object1.otype=='quiz')
			  {
			  if($scope.object1.qtype=='1')
			  {
			  
			  $scope.tsq=$scope.tsq+1;
			  }
			   if($scope.object1.qtype=='0' || $scope.object1.qtype=='2')
			  {
			
			  $scope.tfq=$scope.tfq+1;
			  }
			  }
			  $scope.tobj=$scope.tobj+1;
			 
			  $scope.nuggets[id].nduration = + $scope.nuggets[id].nduration + +$scope.object1.oduration;
			  $scope.tduration =  +$scope.tduration + +$scope.object1.oduration;
			  var minutes = Math.floor($scope.tduration / 60);
			  var seconds = $scope.tduration - minutes * 60;
	  
			$scope.min = minutes;
			$scope.secs = seconds;
			  $scope.$apply();
			 //$scope.object = selectedItem;
		    }, function () {
		     // alert($scope.object);
		    });
		 
		}
		
		$scope.removenugget = function(nugget){
			var index = $scope.nuggets.indexOf(nugget);
			$scope.tduration =  +$scope.tduration - +$scope.nuggets[index].nduration;
			$scope.nuggets.splice(index, 1); 
			 $scope.nuggetslen--;
			 if ($scope.nuggetslen == '-1') {
				 $scope.nuggetslen = 0;
			 }
		}
		$scope.removeobject = function(object, nid){
			var id = +nid - 1;
			var index = $scope.nuggets[id].objects.indexOf(object);
			$scope.nuggets[id].nduration = + $scope.nuggets[id].nduration - +$scope.nuggets[id].objects[index].oduration;
			$scope.tduration = +$scope.tduration - +$scope.nuggets[id].objects[index].oduration;
			$scope.nuggets[id].objects.splice(index, 1); 
			
		};
		
		$scope.uploadfile = function(image, topicid,token){
				
		        var filename = topicid+".png";
		       // image.name = filename;
		        
				
				var apigClient = apigClientFactory.newClient({
				    invokeUrl: $scope.decry.api,
				});
				var params = {};
				
				var body = {
						filetype: image.type,
						filename: filename,
						folder: $scope.folder
						 };
			
				var additionalParams = {
				        headers: {Authorization : token
                        }
					};
					
				apigClient.getpreSignedURLPost(params, body, additionalParams)
				.then(function(result){
				    	
				   var json = JSON.stringify(result.data);
				
				  $scope.upload(image, result.data, topicid);
				 
				    }).catch( function(result){
				    	
				    	var json = JSON.stringify(result);
				    	var json1 = json.toString();
				    	alert('ERROR1'+result);
				    	
				    	
				    });
		      
			
		};
		$scope.upload = function(file,url,topicid) {
			//$scope.objecturl = $sce.trustAsResourceUrl(url);
			
			      // Perform The Push To S3
			      $http.put(url, file, {headers:{'Content-Type': file.type}})
			        .success(function(resp) {
			          
			        //	$window.location.href = '#managetopics';
			        })
			        .error(function(resp) {
			          alert("An Error Occurred Attaching Your File. Please try again or Contact production Support if problem persist");
			        });
			}	
		
$scope.onTagAdded = function(tag, limit) {
	
	if ($scope.stag.length > limit) {
		$scope.stag.pop();
	}
}
$scope.paste = function(event, limit) {
	event.preventDefault();
	
	var ttags = event.originalEvent.clipboardData.getData('text/plain').split(',')
	for (var i = 0; i < ttags.length; i++) {
		
		$scope.stag.push({"text":ttags[i]});
		
	}	
	
	if ($scope.stag.length > limit) {
		$scope.stag.length = 15;
	}	
}	
};

app.controller('addtopicCtrl', addtopicCtrl);
addtopicCtrl.$inject = ['$scope', '$http','$location', '$window','$cookies', '$uibModal','topicjson', 'Upload', '$sce','$crypto','config'];


app.directive('multiSelect', function() {

	  function link(scope, element) {
	    var options = {
	      enableClickableOptGroups: true,
	      onChange: function() {
	        element.change();
	      }
	    };
	    element.multiselect(options);
	  }

	  return {
	    restrict: 'A',
	    link: link
	  };
	});





