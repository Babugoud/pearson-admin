var viewtenantsCtrl = function($scope, $uibModalInstance, $http, $location, $window, $cookies, tenantslist1) {
	$scope.home = function() {
// alert($scope.row.TENANTS)
	$scope.error = false;
	$scope.selectAll = false;
	$scope.select = true;
	$scope.datepicker = false;
	$scope.minDate = new Date();
	$scope.myDate = {
			"date": ""
	}

	$scope.tenants = JSON.parse(tenantslist1);
    $scope.tenants = $scope.tenants.tenants;
    $scope.tenants.unshift({"OID":"ALL INSTITUTIONS","UC":0});
	$scope.orgtenants = angular.copy($scope.tenants);
		$scope.tenantssearch = '';
		$scope.$watch('tenantssearch', function() {
			$scope.tenants = $scope.orgtenants.filter(function(item) {
				return item.toLowerCase().toString().includes($scope.tenantssearch.toLowerCase()) ;  
			 });
			 $scope.tenants.sort();
			 
		 }, true);
	

	$scope.deleteTenants = [];
	$scope.forAllOrgs = {
		value : false,
	  };
	};
	
	$scope.options = [];

	$scope.updateTenant = function(item) {
		
		if ($scope.publishToTenants.includes(item)) {
			var index = $scope.publishToTenants.indexOf(item);
			$scope.publishToTenants.splice(index, 1);
		} else {
			$scope.publishToTenants.push(item);
		}
	}
	  $scope.toggleAll = function() {
		if ($scope.publishToTenants.length === $scope.tenants.length){
			$scope.publishToTenants = [];
		} else {
			$scope.publishToTenants = angular.copy($scope.tenants);
		}
		// for (var i = 0; i < $scope.tenants.length; i++) {
		//   $scope.options[i] = checked;
		//   $scope.publishToTenants.push($scope.options[i]);
		//   $scope.tenants[i].checked = checked;
		// }
		
	  };


	  
	$scope.home();
		
		$scope.publish = function(){
			$scope.loading = true;
			$scope.allTenantsList = angular.copy($scope.publishToTenants);
			if ($scope.row.TENANTS.length > 0) {
				for (var i=0; i<$scope.row.TENANTS.length;i++) {
					if ($scope.publishToTenants.includes($scope.row.TENANTS[i])) {
						$scope.publishToTenants.splice($scope.publishToTenants.indexOf($scope.row.TENANTS[i]), 1);
					} else {
						$scope.deleteTenants.push($scope.row.TENANTS[i]);
					}
				}
			}
			// console.log($scope.publishToTenants);
			// console.log($scope.deleteTenants);
			var list = {
				allTenants: $scope.allTenantsList,
				deleteTenants: $scope.deleteTenants,
				newTenants: $scope.publishToTenants
			};
			$uibModalInstance.close(list);
			// if ($scope.publishToTenants.length > 0) {
			// 	$uibModalInstance.close(list);
			// } else {
			// }
			
        }
        $scope.loadtenant = function(tenant){
            $uibModalInstance.close(tenant);
        };  
        $scope.close = function(){
            $uibModalInstance.close(' ');
        };
};

app.controller('viewtenantsCtrl', viewtenantsCtrl);
viewtenantsCtrl.$inject = ['$scope', '$uibModalInstance', '$http', '$location', '$window','$cookies', 'tenantslist1'];

app.factory("gettenants1", function($window, $q, config,$crypto){
    return {
    	gettenants1: function(){
			var tenants,decry;
    		if(localStorage.getItem("786a2y1e") != null)
            {
               decry = JSON.parse($crypto.decrypt(localStorage.getItem("786a2y1e"), config.key));
            }else
            {
                localStorage.clear();
                $window.location.href = '#login';
            } 
			AWSCognito.config.region =  config.reg;
    	    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    	        IdentityPoolId: decry.iid
    	    });
    	   
    		var poolData = { UserPoolId : decry.uid,
    		        ClientId : decry.cid
    		    };
    		
    		
    		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
    		
    		var cognitoUser = userPool.getCurrentUser();
    		
    	   if (cognitoUser != null && decry.email != null) {
			tenants = getusersession();
    		  
    	    }
    	    else {
    	    	localStorage.clear();
    	    	$window.location.href = '#login';
    	    }
			function getdata(token){
							var apigClient = apigClientFactory.newClient({ invokeUrl: decry.api, });
							var params = {};
			        	    					
			        	    					
							var body = {};
							
							
							var additionalParams = {
						             headers: {Authorization : token
						             }
						       };
							var data =	apigClient.getTenantsPost(params, body, additionalParams)
							.then(function(result){
							    	
								
							   var json = JSON.stringify(result.data);
							   
								var tenant = json.toString();
							    return $q.when(tenant);
							    	
							    }).catch( function(result){
							    	
							    	var json = JSON.stringify(result);
							    	var json1 = json.toString();
							    	localStorage.setItem("activemenu", 'dashboard');
									$window.location.href = '#dashboard';
							    	
							    });
							return $q.when(data);
			}
						
			function getusersession(){
			 	    	 return new Promise((resolve, reject) => {
			 	 			 cognitoUser.getSession((err, session) =>{
			 	 	            if (err) {
			 				    	swal({title: "Oops!", text: "Session has timed out, Please login again.", type: "error",buttonsStyling:false,allowOutsideClick: false,
			 			                allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
			 	 	            	localStorage.clear();
			 	 	            	$window.location.href = '#login';
			 	 	            }else{
			 	 	            	var token = session.idToken.jwtToken;
			 	 	            	var abcc = getdata(token); 
			 	 	            	resolve(abcc)
			 	 	            	return $q.when(abcc);
			 	 	        
			 	 	            }
			 	  		  	});
			 	 		})
			 	    }

    	    
    	    return $q.when(tenants);
    	}
    };
});