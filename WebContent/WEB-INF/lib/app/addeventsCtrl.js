 "use strict";
var addeventsCtrl = function ($scope, $http, $location, $window, $uibModal,$uibModalInstance, NgTableParams, config,$crypto) {
	
	$scope.allcourses = true;
	$scope.listevents = function(){
		$scope.imgurl = config.url;
	    $scope.decry = JSON.parse($crypto.decrypt(localStorage.getItem("786a2y1e"), config.key));
		// $scope.groupid = $scope.decry.tenant;
		var dateObj = new Date();
		dateObj.setDate(dateObj.getDate() - 1);  
		$scope.minDate = dateObj;
		AWSCognito.config.region =  config.reg;
	    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
	        IdentityPoolId: $scope.decry.iid
	    });
	   
		var poolData = { UserPoolId : $scope.decry.uid,
		        ClientId : $scope.decry.cid
		    };
		
		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
		
		var cognitoUser = userPool.getCurrentUser();
		
	    if (cognitoUser != null && $scope.decry.email != null) {
	    	cognitoUser.getSession(function(err, session) {
	            if (err) {
	            	localStorage.clear();
	            	$window.location.href = '#login';
	            }else{
	       
					
		  // $scope.groups =JSON.parse(groupslist);
		  // $scope.groups = $scope.groups.groups;
		//   var org={"GID":'1',"GNAME":'For Organisation'};
		//   $scope.groups.push(org);

		  $scope.orgid = $scope.decry.oid;
		  $scope.orgidlow = $scope.orgid.toLowerCase();
		  
		  if($window.innerWidth > 1024){
			  $scope.secondwidth = +$window.innerWidth - +224;
	    	}else{
	    		$scope.secondwidth = +$window.innerWidth - +65;
	    	}
		  $scope.second = {'width':$scope.secondwidth};
		  $(window).resize(function() {
		
	      $scope.$apply(function() {
	        $scope.windowWidth = $( window ).width();
	        if($scope.windowWidth < 1023){
	        	$scope.secondwidth = +$window.innerWidth - +65;
	    		$scope.second = {'width':$scope.secondwidth};
	        	$scope.sscreen = true;
	    		$scope.lscreen = false;
	        }
	        if($scope.windowWidth > 1024 ){
	        	$scope.secondwidth = +$window.innerWidth - +239;
	    		$scope.second = {'width':$scope.secondwidth};
	        	$scope.sscreen = false;
	    		$scope.lscreen = true;
	        }
	      });
	    });
	    }
	});
		}else{
	    	localStorage.clear();
	    	$window.location.href = '#login';
			}
			if( $scope.allOrg === undefined) {
				$scope.decry["activemenu"] = 'tenants';
			} else {
				$scope.decry["activemenu"] = 'events';
			}
	    
        localStorage.setItem("786a2y1e",$crypto.encrypt(JSON.stringify($scope.decry), config.key));
	    //localStorage.setItem("activemenu", 'contrep');
		window.navigating = false;
		$scope.error = false;
	   $scope.error1 = false;
	};
	
	$scope.listevents();
	
$scope.addevents = function(){
	$scope.error = false;
	$scope.error1 = false;
	$scope.error2 = false;
	$scope.error3 = false;
	$scope.error4 = false;
	$scope.error5 = false;
	$scope.error6 = false;
	var r = new RegExp(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/);
	
	if($scope.title === undefined || $scope.title == '' ||
	 $scope.title.replace(/\s/g, '').length === 0 || $scope.desc == undefined 
	 || $scope.desc.replace(/\s/g, '').length === 0 || $scope.desc == ''
	|| $scope.dates == undefined || $scope.dates == ''  || $scope.dates == null || $scope.dates == 'null' ){
		
		// if($scope.type === undefined || $scope.type === '' ){
		// 	$scope.error2 = true;
		// 	return;
		// }
		if($scope.title === undefined || $scope.title === '' || $scope.title.replace(/\s/g, '').length === 0 ){
			$scope.error = true;
			return;
		}
		
		if($scope.desc === undefined || $scope.desc === '' || $scope.desc.replace(/\s/g, '').length === 0 ){
			$scope.error3 = true;
			return;
		}
		if($scope.dates === undefined || $scope.dates === '' || $scope.dates === null || $scope.dates === 'null' ){
			$scope.error4 = true;
			return;
		}
		// if($scope.groupid === undefined || $scope.groupid === '' ){
		// 	$scope.error6 = true;
		// 	return;
		// }
		
	}else{
		
		if($scope.decry.etype === 'Webinar'){
			if($scope.links === undefined || $scope.links === '' || $scope.links.replace(/\s/g, '').length === 0 ){
				$scope.error1 = true;
				return;
			}
			
			if(!r.test($scope.links)){
				$scope.error5 = true;
				return;
			}
		}
		$scope.getEvents();
	}
  }
  $scope.getEvents =  function(){
	$scope.loading = true;
        var decrypted =$crypto.decrypt(localStorage.getItem("786a2y1e"), config.key);               
        $scope.decry=JSON.parse(decrypted);
        var apigClient = apigClientFactory.newClient({ });
       var params1 = {};
	  //$scope.dates= $('#datetimepicker').val();
	  var a = new Date($scope.dates);
	  var aday = a.getFullYear() + "-" + (a.getMonth() + 1) + "-" + a.getDate();
	   var body={ };
		
	if( $scope.allOrg === undefined) {
		body.oid = $scope.decry.tenant;
	} else {
		body.allOrg = true;
	}

	if($scope.links === undefined){
		body.annjson = {
			"start":aday,
			"title":$scope.title,
			"desc":$scope.desc,
			// "gid":$scope.groupid.GID,
			//"gname":$scope.groupid.GNAME
		};
		body.action = "events";
		
 	} else {
		body.annjson={
			"start":aday,
			"link":$scope.links,
			"title":$scope.title,
			"desc":$scope.desc,
			//"gid":$scope.groupid.GID,
			//"gname":$scope.groupid.GNAME
		};
	 	body.action = "webinar";
	
	   }

    var additionalParams = {};    

   var topics1 = apigClient.addEventPost(params1,body,additionalParams)
               .then(function(result){
            	   
				   var json1 = JSON.stringify(result.data);
				   if( $scope.allOrg === undefined) {
					   
						$scope.decry["status"] = "Events";
						localStorage.setItem("786a2y1e",$crypto.encrypt(JSON.stringify($scope.decry), config.key));
						$window.location.href="#viewtenant";
					} else {
					
						$window.location.href="#viewevents";
					}
                  
                 
                   }).catch( function(result){
                      
                      alert(JSON.stringify(result))
                       
                   });

	}
	
	$scope.deleteEvent = function(row){
		$scope.loading = true;
		var decrypted =$crypto.decrypt(localStorage.getItem("786a2y1e"), config.key);               
        $scope.decry=JSON.parse(decrypted);
        var apigClient = apigClientFactory.newClient({ });
	   var params1 = {};
	
	var additionalParams = {};    
	var body={
		eid: row.eid,
		// gid: $scope.decry.group.GID
		}
		if( $scope.allOrg === undefined) {
			body.oid = $scope.decry.tenant;
		} else {
			body.allOrg = true;
		}
	    if(row.link === undefined){
			body.action = "events"
		  } else {
			body.action = "webinar"
		  }

    var topics1 = apigClient.deleteEventPost(params1,body,additionalParams)
               .then(function(result){
            	  
                   var response = JSON.stringify(result.data);
                   if(response.id=== 0 || response.id=== '0'){
					
				   }else{

				   }
				 
				   $window.location.href="#addevents";

                   }).catch( function(result){  
                      alert(JSON.stringify(result)) 
                   });
	}
	$scope.close = function(){
		$uibModalInstance.dismiss('cancel');
	};
};

app.controller('addeventsCtrl', addeventsCtrl);
addeventsCtrl.$inject = ['$scope', '$http','$location', '$window','$uibModal','$uibModalInstance', 'NgTableParams', 'config','$crypto'];

app.factory("getgroupslist", function($window, $q, config,$crypto){
    return {
    	getgroupslist: function(){
    		var listtopics1,decry;
    		
    		if(localStorage.getItem("786a2y1e") != null)
            {
               decry = JSON.parse($crypto.decrypt(localStorage.getItem("786a2y1e"), config.key));
            }else
            {
                localStorage.clear();
                $window.location.href = '#login';
            }
    		AWSCognito.config.region =  config.reg;
    	    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
    	        IdentityPoolId: decry.iid
    	    });
    	   
    		var poolData = { UserPoolId : decry.uid,
    		        ClientId : decry.cid
    		    };
    		
    		
    		var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
    		
    		var cognitoUser = userPool.getCurrentUser();
    		
    	   if (cognitoUser != null && decry.email != null) {
    		   listtopics1 = getusersession();
    		  
    	    }
    	    else {
    	    	localStorage.clear();
    	    	$window.location.href = '#login';
    	    }
			
			function getdata(token){
						
						var apigClient = apigClientFactory.newClient({invokeUrl: decry.api,});
						  var params = {};

							var body = {
								oid : decry.oid
									 };
							
							var additionalParams = {
						             headers: {Authorization : token
						             }
							   };
							
					var data =	apigClient.getGroupsPost(params, body, additionalParams)
						.then(function(result){
							var json = JSON.stringify(result.data);
							var res= {};
							res.groups = JSON.parse(json);
							return $q.when(JSON.stringify(res));
							
					}).catch( function(result){
						
						var json = JSON.stringify(result);
						var json1 = json.toString();
						return $q.when("hello1");
						
					});
			
			
			return $q.when(data);
			}

			function getusersession(){
						 return new Promise((resolve, reject) => {
							 cognitoUser.getSession((err, session) =>{
								if (err) {
									swal({title: "Oops!", text: "Session has timed out, Please login again.", type: "error",buttonsStyling:false,allowOutsideClick: false,
		     			                allowEscapeKey:false, width: '400px',showConfirmButton: true, confirmButtonText: 'OK', customClass: 'sweetalert-confirmOk',confirmButtonClass:'button1'});
									localStorage.clear();
									$window.location.href = '#login';
								}else{
									var token = session.idToken.jwtToken;
									var abcc = getdata(token); 
									resolve(abcc)
									return $q.when(abcc);
							
								}
							});
						})
					}

    	return $q.when(listtopics1);
    }
    };
});


